
import { default as SelectLokup } from "./default/select/SelectLokup.vue";
import Element from "./default/element/Element";
import InputMask from "./default/element/InputMask";
import { DTable, DynamicForm, Pagination, Exports, Dashboard, SubAba, ButtonAba } from "./default";
import {
    CreateMenu,
    CreateModulo,
    CreatePerfil,
    CreatePermissao,
    CreatePermissaoMenu,
    CreateUsuario,
    CreateUsuarioPerfil,
    EditMenu,
    EditModulo,
    EditPerfil,
    EditPermissao,
    EditPermissaoMenu,
    EditUsuario,
    EditUsuarioPerfil,
    FormMenu,
    FormModulo,
    FormPerfil,
    FormPermissao,
    FormPermissaoMenu,
    FormUsuario,
    FormUsuarioPerfil,
    LayoutBuilder,
    Menu,
    Modulo,
    Perfil,
    Permissao,
    PermissaoMenu,
    Usuario,
    UsuarioPerfil
} from "./configuracao";

import VueTabsChrome from 'vue-tabs-chrome';

export default {
    // default
    btn: uiv.Btn,
    selectlokup: SelectLokup,
    elementd: Element,
    LocalQuillEditor: VueQuillEditor.quillEditor,
    dtable: DTable,
    pagination: Pagination,
    'date-picker': uiv.DatePicker,
    dropdown: uiv.Dropdown,
    tabs: uiv.Tabs,
    tab: uiv.Tab,
    modal: uiv.Modal,
    inputmask: InputMask,
    dynamicform: DynamicForm,
    draggable: vuedraggable,
    exports: Exports,
    vueTabsChrome: VueTabsChrome.VueTabsChrome,

    // Aba
    dashboard: Dashboard,
    buttonAba: ButtonAba,
    subaba: SubAba,

    // Configuracao - Menu
    menuc: Menu,
    formmenu: FormMenu,
    editmenu: EditMenu,
    createmenu: CreateMenu,

    // Configuracao - Usuario
    usuario: Usuario,
    formusuario: FormUsuario,
    editusuario: EditUsuario,
    createusuario: CreateUsuario,

    // Configuracao - Perfil
    perfil: Perfil,
    formperfil: FormPerfil,
    createperfil: CreatePerfil,
    editperfil: EditPerfil,

    // Configuracao - Modulo
    modulo: Modulo,
    formmodulo: FormModulo,
    createmodulo: CreateModulo,
    editmodulo: EditModulo,

    // Configuracao - Permissao
    permissao: Permissao,
    formpermissao: FormPermissao,
    createpermissao: CreatePermissao,
    editpermissao: EditPermissao,

    // Configuracao - Menu - Permissao Menu
    permissaomenu: PermissaoMenu,
    formpermissaomenu: FormPermissaoMenu,
    createpermissaomenu: CreatePermissaoMenu,
    editpermissaomenu: EditPermissaoMenu,

    // Configuracao - Usuario - Usuario Perfil
    usuarioperfil: UsuarioPerfil,
    formusuarioperfil: FormUsuarioPerfil,
    createusuarioperfil: CreateUsuarioPerfil,
    editusuarioperfil: EditUsuarioPerfil,

    // Layout Builder
    layoutbuilder: LayoutBuilder,
}