export default class OnClickAba {
    constructor() {
        this.$abaService = Vue.prototype.$abaService;
    }

    click(menu){
        if(menu.componente) {
            this.$abaService.addAba(menu.nome,'component',menu.componente);
        } else if(menu.funcao != ''&&menu.funcao!=null) {
            eval(menu.funcao);
        } else {
            this.$abaService.addAba(menu.nome,'component','subaba',{ idmenu: menu.id });
        }
    }

    remover(code) {
        this.$abaService.remover(code);
    }
}