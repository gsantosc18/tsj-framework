import Axios from "axios";
import RouteService from "./RouteService";

const $route = new RouteService;

export default class MenuService
{   
    constructor() {
        this.menus = null;
    }
    all(){
        if(this.menus == null) {
            this.menus = Vue.prototype.$externalService.get('submenu.all');
        }
        return this.menus;
    }

    getSubMenu(menu) {
        if(!menu) { 
            return;
        }
        return Vue.prototype.$externalService.get('submenu.all',{ menu });
    }
}