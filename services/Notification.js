function arrayToString(data,divider) {
    return data.join(divider||"<br>");
}

export default class {
    show(type,message) {
        Vue.prototype.$swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 5000,
                timerProgressBar: true
            }).fire({
                icon: type,
                title: message
            });
    }

    showData(type,text,data) {
        let message = "";
        if(text) {
            message = message +"<br/>";
        }
        if(data) {
            message = message + arrayToString(data);
        }
        this.show(type,message);
    }

    dialog(icon,title,data) {
        let html = data;
        if(typeof data == "object") {
            html = arrayToString(data);
        }
        return Vue.prototype.$swal.fire({
            icon,
            title,
            html,
            confirmButtonText: "Fechar"
        });
    }

    confirm(type,title,message,confirm) {
        Vue.prototype.$swal.fire({
            title: title,
            text: message,
            icon: type,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim',
            cancelButtonText: 'Não'
        }).then(r=>{ if (r.value) { confirm() } });
    }
}
