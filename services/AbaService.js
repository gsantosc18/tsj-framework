const COMPONENT = 'component';
const HTML = 'html';
export default class AbaService {
    constructor() {
        this.count = 2;
        this.listener = null;
    }

    addAba(titulo,type,conteudo,chaves) {
        this.listener({
            label: titulo,
            type: type,
            key: "aba"+(++this.count),
            conteudo: conteudo,
            ativo: true,
            chaves: chaves
        });
    }

    addComponent(titulo,component,chave) {
        this.addAba(titulo,COMPONENT, component, chave);
    }
    addHtml(titulo,conteudo) {
        this.addAba(titulo,HTML, conteudo,null);
    }

    setListener(callback) {
        this.listener = callback;
    }

    tratanome(nome) {
        return nome.normalize("NFD").replace(/[^a-zA-Zs]/g, "");
    }
}