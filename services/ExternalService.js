import Axios from "axios";
import RouteService from "./RouteService";
import { default as errorHandler } from './ErrorService';

var CACHE = {};

const $routes = new RouteService();

const get = (route,params) => {
    if(!CACHE[route]) {
        CACHE[route] = Axios.get($routes.get(route),{ params:{...params} });
    }
    return CACHE[route];
}

const findCache = (route) => {
    for(let c in CACHE) {
        if(c == route) {
            return CACHE[c];
        }
    }
}

export default class ExternalService{
    addRoutes(routes) {
        $routes.addRoutes(routes);
    }
    
    getData(route,params) {
        let cache = null;
        if(!params) {
            cache = findCache(route);
            if (!cache) {
                cache = get(route);
            }
        } else {
            cache = get(route,params).catch( errorHandler );
        }
        return cache;
    }

    getLokup(route,id) {
        return this.getId(route,id).catch( errorHandler );
    }

    getId(route,id) {
        if(id == undefined) return Promise.resolve({data:null});
        let url = `${$routes.get(route)}/${id}`,
            cache = findCache(url);
        if (!cache) {
            cache = Axios.get(url).catch( errorHandler );
        }
        return cache;
    }

    get(route,params,response) {
        return Axios.get($routes.get(route),{ params:{...params} }).then( response || (()=>{}), errorHandler );
    }

    getExternal(route,params) {
        return Axios.get($routes.get(route),{ params:{...params} });
    }

    post(route,params,response) {
        return Axios.post($routes.get(route),params).then( response || (()=>{}), errorHandler );
    }

    put(route,id,params,response) {
        return Axios.put(`${$routes.get(route)}/${id}`,params).then( response || (()=>{}), errorHandler )
    }

    delete(route,id,params,response) {
        return Axios.delete(`${$routes.get(route)}/${id}`,params).then( response || (()=>{}), errorHandler );
    }
}
