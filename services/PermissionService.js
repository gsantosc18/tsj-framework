import Axios from "axios";

export default class PermissionService {

    constructor() {
        Object.assign(this,{permission:{}});
    }

    async initialize(instance) {
        if(instance.permissions == null) {
            instance.permissions = await Axios.post('permissions').then(r => r.data);
            return instance.permissions;
        }
    }

    async has(aplicacao) {
        for(let i in this.permissions) {
            let m = this.permissions[i];
            if(m.aplicacao == aplicacao) {
                return m;
            }
        }
        return {};
    }

    async getPermissions(data,response) {
        const DF = {
            create: false,
            update: false,
            delete: false,
            select: false,
            show: false,
            can: (name) => {this[name]}
        };
        let permissions = {};
        for(let p of data) {
            permissions[p] = $.extend({},DF,await this.has(p));
        }
        if(response && typeof response == "function") {
            response(permissions);
        }
        return permissions;
    }

    get(aplication) {
        const DF = {
            create: false,
            update: false,
            delete: false,
            select: false,
            show: false,
            can: function(name) {return this[name]}
        };
        let permission = {};

        for(let i in this.permissions) {
            let m = this.permissions[i];
            if(m.aplicacao == aplication) {
                permission = m;
            }
        }

        return $.extend({},DF,permission);
    }
}
