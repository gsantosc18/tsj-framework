import { Notification } from './index';

export default function (r) {
    const notification = new Notification;
    const response = r.response;
    
    console.error(r);

    if(!response.data || typeof r != "object") {
        notification.dialog("error", "Houve um erro interno do sistema, contate o administrador.", null);
        return;
    }
    if (response || (!response.data.message && !response.data.data && !response.data.errors)) {
        notification.dialog("error", "Houve um erro interno do sistema, contate o administrador.", `<code>${JSON.stringify(response.data)}</code>`);
        return;
    }
    const internal = response.data;
    notification.dialog("error", internal.message, Object.assign([], internal.data, internal.errors));
}
