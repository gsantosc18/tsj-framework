let ROUTES = {
    // Usuario
    usuario: 'api/usuario',
    perfil: 'api/perfil',
    changeperfil: 'changeperfil',
    usuarioperfil: 'api/usuarioperfil',
    selectperfil: 'external/perfil',
    // Permissao
    permissao: 'api/permissao',
    selectmodulo: 'external/modulo',
    permissaomenu: 'api/permissaomenu',
    modulo: 'api/modulo',
    // Menu
    menu: {
        lookup: 'menu/lookup',
        crud: 'api/menu',
    },
    submenu: {
        onlyActive: 'menu/onlyActive',
        all: 'menu/all',
    },
};

let ROOT_URL = "";
export default class RouteService {
    get(route) {
        let router = null;
        let subRoutes = route.split(".");

        for (let r of subRoutes) {
            if(router!=null) {
                if(!router[r]) {
                    throw "A rota não esta definida: "+route;
                }
                router = router[r];
            } else if (!ROUTES[r]) {
                throw "A rota não esta definida: "+route;
            } else {
                router = ROUTES[r];
            }
        }

        return ROOT_URL+router;
    }

    addRoutes(route) {
        ROUTES = Object.assign(ROUTES, route);
    }

    setHost(host) {
        ROOT_URL = host
    }
}
