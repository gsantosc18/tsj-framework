import Axios from "axios";

export default class InfoUser {
    constructor() {
        this.infoUser = null;
        this.perfils = null;
    }
    get() {
        if (this.infoUser == null) {
            this.infoUser = axios.get('userinfo');
        }
        return this.infoUser;
    }
    async getPerfils() {
        if(this.perfils == null) {
            this.perfils = axios.get('usuarioperfil');
        }
        return this.perfils;
    }
}