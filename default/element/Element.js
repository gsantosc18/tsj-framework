export default {
    template: `
  <component
    :is="component"
    v-bind="keys"
    v-model="content"
    :value="content"
    :type="type"
    :checked="component=='input'&&type=='checkbox'&&content==1?true:false"
    @input="handleInput"
    @change="handleChange"
    @focus="handleFocus"
    @blur="handleBlur"
  ><slot></slot></component>
  `,
    props: {
        component: { type: String, required: true },
        value: { default: "" },
        type: { default: "" },
        keys: {
            type: [Array, Object],
            default: () => ({})
        }
    },
    data() {
        return {
            content: ""
        }
    },
    watch: {
        $props: {
            immediate: true,
            deep: true,
            handler(e) {
                this.content = e.value;
            }
        }
    },
    methods: {
        handleInput(e) {
            this.$emit('input', e);
        },
        handleChange(e) {
            this.$emit('change', e);
        },
        handleFocus(e) {
            this.$emit('focus', e);
        },
        handleBlur(e) {
            this.$emit('blur', e);
        },
    },
}
