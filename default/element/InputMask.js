export default {
    props: ['mask','content',"value","placeholder"],
    template: `<input type="text"
        v-model="v"
        v-mask="mask"
        :placeholder="placeholder"
        @input="handleInput"
        @change="handleChange"
        @focus="handleFocus"
        @blur="handleBlur">`,
    data() {
        return {
            v: ""
        }
    },
    watch: {
        $props : {
          immediate: true,
          deep: true,
          handler(e) {
            this.v = e.value;
          }
        }
      },
    methods: {
        handleInput(e){
            this.$emit('input',e);
        },
        handleChange(e) {
            this.$emit('change',e);
        },
        handleFocus(e) {
            this.$emit('focus',e);
        },
        handleBlur(e) {
            this.$emit('blur',e);
        },
    },
}
