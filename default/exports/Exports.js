import RouteService from './../../services/RouteService';
const $routes = new RouteService();
export default {
    props: ['data', 'route'],
    data() {
        return {
            modalExport: false,
            buttons: [
                {
                    alias: 'Pdf',
                    type: 'pdf'
                },
                {
                    alias: 'Excel',
                    type: 'excel'
                }
            ]
        }
    },
    methods: {
        actionPerform(type) {
            let paramQuery = $.param(this.data);
            let route = $routes.get(this.route);
            let fullURL = `${route}/export/${type}?${paramQuery}`;
            window.open(fullURL, '_blank');
        }
    }
}
