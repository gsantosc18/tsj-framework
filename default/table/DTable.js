export default {
    props: {
        dados: {
            type: [Object,Array]
        },
        url: {
            type: String
        },
        columns: {
            type: [Object,Array]
        },
        headers: {
            type: [Object,Array]
        },
        classTh: {
            type: String
        },
        classTd: {
            type: String
        },
        viewTotals: {
            type: Array,
            default: () => ([ 5, 15, 25, 50, 100, 200, 500 ])
        },
        animationVisible: {
            type: Boolean,
            default: false
        },
        totalPage: {
            type: Number,
            default: 0
        },
        value: {
            type: Number,
            default: 1
        },
        footer: {
            type: Boolean,
            default: true
        },
        perPage: {
            type: Number,
            default: 15
        }
    },
    data() {
        return {
            page: 1,
            perPageSelect: 15
        }
    },
    watch: {
        page: function(){
            this.handlerChange();
        },
        perPageSelect: function() {
            this.handlerChange();
        }
    },
    created() {
        this.page = this.validValueOrDefault(this.value,1);
        this.perPageSelect = this.validValueOrDefault(this.perPage,15);
    },
    methods: {
        getTypeColumn(column){
            return typeof column;
        },
        getContent(dados,column) {
            if(!column) {
                return column;
            }
            let fragment = column.split(".");
            if(fragment.length > 1) {
                let response = null;
                for(let i of fragment) {
                    if(!response) {
                        response = dados[i];
                        if(response == null || response == undefined) {
                            break;
                        }
                    } else {
                        response = response[i];
                    }
                }
                return response;
            }
            return dados[column];
        },
        trataCan(column) {
            if(typeof column == 'object') {
                if(column.can) {
                    return column.can();
                }
                return false;
            }
            return true;
        },
        validValueOrDefault(value,vlDefault){
            return value?value:vlDefault;
        },
        handlerChange() {
            this.$emit("changeTable",{
                perPage: this.perPageSelect,
                currentPage: this.page
            });
            this.$emit("input",this.page);
        }
    },
}
