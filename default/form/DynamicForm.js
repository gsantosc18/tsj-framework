export default {
    props: {
        shematics: {
            type: [Array,Object],
            required: true
        },
        events: {
            type: [Array,Object],
            default: ()=>({})
        },
        dados: {
            type: [Array,Object],
            default: ()=>({})
        },
        urlokups: {
            type: [Array,Object]
        },
        actionlokups: {
            type: [Array,Object]
        },
        classlokup: {
            type: String,
            default: 'selectlokup'
        }
    },
    data() {
        return {
            values: {},
            lokups: {}
        }
    },
    watch: {
        $props: {
            immediate: true,
            deep: true,
            handler(e) {
                for(let i in e.dados) {
                    this.values[i] = e.dados[i];
                }
                this.$emit('input',this.values);
            }
        }
    },
    async mounted() {
        this.values = this.loadData();
        this.lokups = await this.loadLokups();
        this.loadActionLokup();
    },
    methods: {
        loadData() {
            let data = {},
                dados = this.$props.dados;
            for(let j of this.shematics) {
                for(let k of j.childs) {
                    let v = k.keys;
                    data[k.model] = undefined;
                    if(k.value) {
                        data[k.model] = k.value;
                    } else if(v && v.selected) {
                        data[k.model] = v.selected;
                    }
                }
            }
			for(let i in dados) {
				data[i] = dados[i];
			}
			return data;
		},
		async loadLokups() {
            let urls = this.urlokups,
                lokups = {};
            for(let j of this.shematics) {
                for(let k of j.childs) {
                    if(k.component==this.classlokup) {
                        lokups[k.model] = [];
                    }
                    if(k.keys && k.keys.options) {
                        lokups[k.model] = k.keys.options;
                    }
                }
            }
            for(let i in urls) {
                let index = urls[i];
                if(!this.isLokups(index)) {
                    console.error('O indice:'+index+' não um component Lokups');
                    break;
                }
                if(typeof index == 'object' && index.target && index.request) {
                    await index.request()
                        .then( d => lokups[index.target] = d.data, error => {} );
                    continue;
                }
                await this.$externalService
                    .getData(index)
                    .then( d => lokups[index] = d.data, error => {} );
            }
            return lokups;
        },
        loadActionLokup(){
            const allChilds = this.getAllChilds();
            for(let c of allChilds) {
                this.tratarActionLokups(c.model);
            }
        },
        getAllChilds() {
            let childs = [];
            for(let j of this.shematics) {
                for(let k of j.childs) {
                    childs.push(k);
                }
            }
            return childs;
        },
        isLokups(model){
            for(let j of this.shematics) {
                for(let k of j.childs) {
                    if(k.model == model || ( model.target && k.model == model.target) ) {
                        return true;
                    }
                }
            }
        },
        tratarChave(child){
			let data = {
				options: {},
				selected: ""
			};
			if (child.keys) {
				$.extend(data,child.keys);
			}
			if(this.lokups[child.model]) {
				data.options = this.lokups[child.model];
			}
			if (this.values[child.model]) {
				data.selected = this.values[child.model];
            }
			data.name = child.model;
			return data;
        },
        tratarEvent(event,on,child,index) {
			if( this.events[child.model] && this.events[child.model][on] ) {
				this.events[child.model][on](event.target.value,child,event,index);
            }
            this.tratarSpecificEvent(event,on,child);
            this.tratarActionLokups(child.model);
        },
		tratarSpecificEvent(event,on,child) {
			switch (on) {
				case 'change':
				case 'input':
                    this.values[child.model] = this.getEventValue(child.component,child,event);
				break;
            }
            this.$emit('input',this.values);
        },
        async tratarActionLokups(model){
            const actions = this.actionlokups;
            if (!actions) {
                return;
            }
            const value = this.values[model];
            for(let action of actions) {
                if( action.model == model ) {
                    this.lokups[action.target] = await action.action(value).then(response => response.data);
                }
            }
        },
		getEventValue(component,child,response) {
            if (component == 'LocalQuillEditor') {
                return response.html;
            }

            if(component=='input' && child.type == 'checkbox') {
                let isChecked = response.target.checked;
                return isChecked ? child.checked : child.unchecked;
            }

            return response.target.value;
		},
		setValue(event,index) {
			this.setValueParam(index, event.target.value);
		},
		setValueParam(index,value) {
			this.$refs.params[index] = value;
        }
    },
}
