import Vue from "vue";
// Importa os componentes
import { VueMaskDirective } from 'v-mask'
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
// import _init_ from './components';
import { Event } from './event';
import { AbaService, MenuService, InfoUser, ExternalService, Notification, PermissionService } from './services';
import Register from "./Register";
// Plugins adicionais
Vue.use(VueSweetalert2);
Vue.directive('mask', VueMaskDirective);

Vue.prototype.$abaService = new AbaService;
Vue.prototype.$menuService = new MenuService();
Vue.prototype.$infoUser = new InfoUser();
Vue.prototype.$fireEvent = new Event();
Vue.prototype.$externalService = new ExternalService();
Vue.prototype.$route = new ExternalService();
Vue.prototype.$notification = new Notification();
Vue.prototype.$broadcast = new Vue();
Vue.prototype.$permission = new PermissionService();

export default Register;
