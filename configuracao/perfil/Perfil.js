export default {
    data() {
        return {
            dados: [],
            currentPage: 1,
            totalPage: 0,
            headers: ['...','nome','Cadastro','situação'],
            columns: [
                {
                    title: '<i class="fa fa-pencil"></i>',
                    component: 'btn',
                    can: ()=>(this.$permission.get('perfil').can('show')),
                    event: {
                        click: (data) => {
                            this.$abaService.addComponent(
                                `Editar Perfil: ${data.nome}`,'editperfil',{ dados:data }
                            );
                        }
                    },
                    chaves: (data) => {
                        return {
                            data: data,
                            type : "link",
                            size: "xs"
                        };
                    }
                },
                'nome',
                'data_cadastro',
                'status'
            ]
        }
    },
    watch: {
        currentPage: function () {
            this.find();
        }
    },
    mounted() {
        this.find();
        this.$broadcast.$on('updatePerfil',()=>this.find());
    },
    methods: {
        find() {
            const data = {
                start: this.currentPage - 1,
                qtd: 15
            };
            this.$externalService.get('perfil',data,r=>{
                this.dados=this.trataData(r.data.data);
                this.totalPage = r.data.pages;
                this.sumTotalRecords = r.data.count;
            });
        },
        trataData(data) {
            return data.map((e)=>{
                return {...e,
                    status:this.getBadgeStatus(e.ativo),
                    data_cadastro: this.formatData(e.created_at)
                }
            });
        },
        getBadgeStatus(status) {
            if(status == 1) {
                return '<span class="label label-success btn-sm">ATIVO</span>';
            }
            return '<span class="label label-danger">INATIVO</span>';
        },
        formatData(date) {
            if(!date) return "";
            return moment(date).format('DD/MM/YYYY HH:mm:ss');
        }
    },
}
