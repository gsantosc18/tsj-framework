export default {
	props: {
		dados: {
			type: Object,
			default: () => ({})
		},
		haveSave:{
			type: Boolean,
			default: false
		}
	},
	data() {
		return {
			shematics: require("./json/shematics.json"),
			values: {},
			lokups: [],
			actionLokups: [
			],
		}
	},
	methods: {
		save() {
			this.$emit('save', this.values);
		}
	},
}
