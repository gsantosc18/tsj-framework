export default {
    data() {
        return {
            dados: [],
            currentPage: 1,
            totalPage: 0,
            perpage: 15,
            headers: ['...','permissao','nome','Cadastro','situação'],
            columns: [
                {
                    title: '<i class="fa fa-pencil"></i>',
                    component: 'btn',
                    can: ()=>(this.$permission.get('menu').can('show')),
                    event: {
                        click: (data) => {
                            this.$abaService.addComponent(
                                `Editar Perfil: ${data.nome}`,'editmodulo',{ dados:data }
                            );
                        }
                    },
                    chaves: (data) => {
                        return {
                            data: data,
                            type : "link",
                            size: "xs"
                        };
                    }
                },
                {
                    title: '<i class="fa fa-lock"></i>',
                    component: 'btn',
                    can: ()=>(this.$permission.get('permissao').can('select')),
                    event: {
                        click: (data) => {
                            this.$abaService.addComponent(
                                `Permissões: ${data.nome}`,'permissao',{ filter:{ modulo:data.id } }
                            );
                        }
                    },
                    chaves: (data) => {
                        return {
                            data: data,
                            type : "warning",
                            size: "xs"
                        };
                    }
                },
                'nome',
                'data_cadastro',
                'status'
            ]
        }
    },
    watch: {
        currentPage: function () {
            console.log('alterou');
            this.find();
        }
    },
    mounted() {
        this.find();
        this.$broadcast.$on('updateModulo',()=>this.find());
    },
    methods: {
        find() {
            const data = {
                start: this.currentPage - 1,
                qtd: this.perpage
            };
            this.$externalService.get('modulo',data,r=>{
                this.dados=this.trataData(r.data.data);
                this.totalPage = r.data.pages;
                this.sumTotalRecords = r.data.count;
            });
        },
        trataData(data) {
            return data.map((e)=>{
                return {...e,
                    status:this.getBadgeStatus(e.ativo),
                    data_cadastro: this.formatData(e.created_at)
                }
            });
        },
        getBadgeStatus(status) {
            if(status == 1) {
                return '<span class="label label-success btn-sm">ATIVO</span>';
            }
            return '<span class="label label-danger">INATIVO</span>';
        },
        formatData(date) {
            if(!date) return "";
            return moment(date).format('DD/MM/YYYY HH:mm:ss');
        }
    },
}
