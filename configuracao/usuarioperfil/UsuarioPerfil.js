export default {
    props:{
        usuario: [Array,Object]
    },
    data() {
        return {
            dados: [],
            currentPage: 1,
            totalPage: 0,
            headers: ['...','perfil','Cadastro','sitação'],
            columns: [
                {
                    title: '<i class="fa fa-pencil"></i>',
                    component: 'btn',
                    can: ()=>(this.$permission.get('usuarioperfil').can('show')),
                    event: {
                        click: (data) => {
                            this.$abaService.addComponent(
                                `Editar Perfil: ${data.perfils.nome} do ${data.nome_usuario}`,'editusuarioperfil',{ dados:data }
                            );
                        }
                    },
                    chaves: (data) => {
                        return {
                            data: data,
                            type : "link",
                            size: "xs"
                        };
                    }
                },
                'perfils.nome',
                'data_cadastro',
                'status'
            ],
        }
    },
    watch: {
        currentPage: function () {
            this.find();
        }
    },
    mounted() {
        this.find();
        this.$broadcast.$on('updateUsuarioPerfil',()=>this.find());
    },
    methods: {
        find() {
            const data = {
                start: this.currentPage - 1,
                qtd: 15,
                usuario: this.usuario.id
            };
            this.$externalService.get('usuarioperfil',data,r=>{
                this.dados=this.trataData(r.data.data);
                this.totalPage = r.data.pages;
                this.sumTotalRecords = r.data.count;
            });
        },
        trataData(data) {
            return data.map((e)=>{
                return {...e,
                    status:this.getBadgeStatus(e.ativo),
                    data_cadastro: this.formatData(e.created_at)
                }
            });
        },
        getBadgeStatus(status) {
            if(status == 1) {
                return '<span class="label label-success btn-sm">ATIVO</span>';
            }
            return '<span class="label label-danger">INATIVO</span>';
        },
        formatData(date) {
            if(!date) return "";
            return moment(date).format('DD/MM/YYYY HH:mm:ss');
        }
    },
}
