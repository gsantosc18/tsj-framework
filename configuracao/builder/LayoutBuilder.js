export default {
    props: {
        shematics: {
            type: [Array,Object],
            default: ()=>([])
        },
        haveSave: {
            type: Boolean,
            default: false
        }
    },
    data() {
        return {
            list: [
                { name: "Seção", type: "section" },
                { name: "Input Text", type: "inputmask" },
                { name: "Button", type: "btn" },
                { name: "Textarea", type: "textarea" },
                { name: "Select", type: "selectlokup" },
                { name: "Editor", type: "LocalQuillEditor" },
            ],
            importedShematics: [],
            modalSection: false,
            modalChild: false,
            modalImport: false,
            modalPreview: false,
            sectionEditing: {
                shematics: {}
            }
        };
    },
    computed: {
        valueString() {
            return JSON.stringify(this.shematics, null, 2);
        }
    },
    watch: {
        importedShematics: function(value) {
            this.shematics = JSON.parse(value);
        }
    },
    methods: {
        removeAt(list,idx) {
            list.splice(idx, 1);
        },
        onClone(element) {
            const type = element.type;
            return type == 'section'?{
                    titulo: "Seção",
                    class: "form-group col-md-12",
                    childs: []
                    } : {
                        label: "Elemento",
                        component: type,
                        class: "col-md-3 form-group",
                        elemClass: "form-control",
                        model: "variavel_aplicacao",
                        keys: [],
                        ...this.mapChild(type)
                    };

        },
        editSection(shematics,id) {
            this.modalSection = true;
            this.sectionEditing = {
                id,
                shematics
            };
        },
        editChild(shematics,id) {
            this.modalChild = true;
            this.sectionEditing = {
                id,
                shematics
            };
        },
        mapChild(type) {
            switch (type) {
                case 'LocalQuillEditor':
                        return {
                            class: "col-md-12",
                            elemClass: "",
                            keys: {
                                options: {
                                    theme: "snow",
                                    modules: {
                                        toolbar: [["bold", "italic", "underline"]]
                                    }
                                }
                            }
                        };
                case 'select':
                    return {
                        keys: {
                            options: [
                                {
                                    "id": 1,
                                    "name": "Sim"
                                },
                                {
                                    "id": 2,
                                    "name": "Não"
                                },
                            ],
                            selected: 1
                        }
                    };
            }
            return {};
        },
        changeValue(event) {
            const value = event.target.value;
            this.sectionEditing.shematics.keys = value || [];
        }
    }
}
