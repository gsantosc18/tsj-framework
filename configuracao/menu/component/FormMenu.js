export default {
	props: {
		dados: {
			type: Object,
			default: () => ({})
		},
		menu: [String, Number],
		haveSave:{
			type: Boolean,
			default: false
		}
	},
	data() {
		return {
			shematics: require("./json/shematics.json"),
			values: {},
			lokups: [
				{
                    target: 'submenu',
                    request: ()=>{
						let menu = this.menu;
                        return this.$externalService.getExternal('menu.lookup',{menu});
                    }
                },
			],
			actionLokups: [
			],
		}
	},
	mounted() {
		console.log(this.dados, this.menu);
	},
	methods: {
		save() {
			this.$emit('save', this.values);
		}
	},
}
