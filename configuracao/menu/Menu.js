export default {
    props: {
        menu: {
            type: [String,Number],
            default: ()=>(null)
        }
    },
    data() {
        return {
            dados: [],
            headers: ['...','submenu','permissão','icone','nome','descricao','Ordem','Cadastro','situação'],
            columns: [
                {
                    title: '<i class="fa fa-pencil"></i>',
                    component: 'btn',
                    can: ()=>(this.$permission.get('menu').can('show')),
                    event: {
                        click: (data) => {
                            this.$abaService.addComponent(
                                `Editar Menu: ${data.nome}`,'editmenu',{ dados:data }
                            );
                        }
                    },
                    chaves: (data) => {
                        return {
                            data: data,
                            type : "link",
                            size: "xs"
                        };
                    }
                },
                {
                    title: '<i class="fa fa-outdent"></i>',
                    component: 'btn',
                    can: ()=>(this.$permission.get('menu').can('select')),
                    event: {
                        click: (data) => {
                            this.$abaService.addComponent(
                                `Sub Menu: ${data.nome}`,'menuc',{
                                    menu: data.id
                                }
                            );
                        }
                    },
                    chaves: (data) => {
                        return {
                            data: data,
                            type : "primary",
                            size: "xs",
                            title: "Ver os Submenus"
                        };
                    }
                },
                {
                    title: '<i class="fa fa-lock"></i>',
                    component: 'btn',
                    can: ()=>(this.$permission.get('permissaomenu').can('select')),
                    event: {
                        click: (data) => {
                            this.$abaService.addComponent(
                                `Permissao Menu: ${data.nome}`,'permissaomenu',{
                                    menu: this.menu,
                                    idmenu: data.id
                                }
                            );
                        }
                    },
                    chaves: (data) => {
                        return {
                            data: data,
                            type : "warning",
                            size: "xs",
                            title: "Ver os Permissões do Menu"
                        };
                    }
                },
                'iconeHtml',
                'nome',
                'descricao',
                'ordem',
                'data_cadastro',
                'status'
            ]
        }
    },
    mounted() {
        this.find();
        this.$broadcast.$on('updateMenu',()=>this.find());
    },
    methods: {
        find() {
            let menu = this.menu;
            this.$externalService.get('submenu.all',{menu},r=>this.dados=this.trataData(r.data));
        },
        trataData(data) {
            return data.map((e)=>{
                return {...e,
                    status:this.getBadgeStatus(e.ativo),
                    iconeHtml: this.getIcone(e.icone),
                    data_cadastro: this.formatData(e.created_at)
                }
            });
        },
        getIcone(icone) {
            return `<i class="fa fa-${icone}"></i>`;
        },
        getBadgeStatus(status) {
            if(status == 1) {
                return '<span class="label label-success btn-sm">ATIVO</span>';
            }
            return '<span class="label label-danger">INATIVO</span>';
        },
        formatData(date) {
            return moment(date).format('DD/MM/YYYY HH:mm:ss');
        }
    },
}
