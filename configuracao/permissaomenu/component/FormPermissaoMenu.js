export default {
	props: {
		dados: {
			type: Object,
			default: () => ({})
		},
        menu: [String, Number],
		haveSave:{
			type: Boolean,
			default: false
		}
	},
	data() {
		return {
			shematics: require("./json/shematics.json"),
			values: {},
			lokups: [
				{
                    target: 'menu',
                    request: ()=>{
						let menu = this.menu;
                        return this.$externalService.getExternal('menu.lookup',{menu});
                    }
				},
				{
                    target: 'perfil',
                    request: ()=>{
                        return this.$externalService.getData('selectperfil');
                    }
                },
			],
			actionLokups: [
			],
		}
	},
	methods: {
		save() {
			this.$emit('save', this.values);
		}
	},
}
