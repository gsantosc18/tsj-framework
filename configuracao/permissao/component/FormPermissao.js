export default {
	props: {
		dados: {
			type: Object,
			default: () => ({})
        },
        haveSave:{
			type: Boolean,
			default: false
		}
	},
	data() {
		return {
			shematics: require("./json/shematics.json"),
			values: {},
			lokups: [
				{
					target: 'perfil',
					request: ()=>{
						let menu = this.menu;
                        return this.$externalService.getExternal('selectperfil',{menu});
                    }
				},
				{
					target: 'modulo',
					request: ()=>{
						let menu = this.menu;
                        return this.$externalService.getExternal('selectmodulo',{menu});
                    }
				}
			],
			actionLokups: [],
			events: []
		}
	},
	methods: {
		save() {
			this.$emit('save', this.values);
		}
	},
}
