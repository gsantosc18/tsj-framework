export default {
    props: {
        filter: {
            type: [Array,Object],
            default:()=>({})
        }
    },
    data() {
        return {
            dados: [],
            currentPage: 1,
            totalPage: 0,
            headers: ['...','Perfil','modulo','habilidades','Cadastro','situação'],
            columns: [
                {
                    title: '<i class="fa fa-pencil"></i>',
                    component: 'btn',
                    can: ()=>(this.$permission.get('permissao').can('show')),
                    event: {
                        click: (data) => {
                            this.$abaService.addComponent(
                                `Editar Permissão`,'editpermissao',{ dados:data }
                            );
                        }
                    },
                    chaves: (data) => {
                        return {
                            data: data,
                            type : "link",
                            size: "xs"
                        };
                    }
                },
                'perfils.nome',
                'modulos.nome',
                'habilidades',
                'data_cadastro',
                'status'
            ]
        }
    },
    watch: {
        currentPage: function () {
            this.find();
        }
    },
    mounted() {
        this.find();
        this.$broadcast.$on('updatePermissao',()=>this.find());
    },
    methods: {
        find() {
            const data = {
                start: this.currentPage - 1,
                qtd: 15,
                ...this.filter
            };
            this.$externalService.get('permissao',data,r=>{
                this.dados=this.trataData(r.data.data);
                this.totalPage = r.data.pages;
                this.sumTotalRecords = r.data.count;
            });
        },
        trataData(data) {
            return data.map((e)=>{
                return {...e,
                    status:this.getBadgeStatus(e.ativo),
                    habilidades: this.trataFieldsPermissions(e),
                    data_cadastro: this.formatData(e.created_at)
                }
            });
        },
        getBadgeStatus(status) {
            if(status == 1) {
                return '<span class="label label-success btn-sm">ATIVO</span>';
            }
            return '<span class="label label-danger">INATIVO</span>';
        },
        formatData(date) {
            if(!date) return "";
            return moment(date).format('DD/MM/YYYY HH:mm:ss');
        },
        can(field) {
            return field == 1;
        },
        trataFieldsPermissions(data) {
            let fields = "";
            if(this.can(data.select)) {
                fields += this.getIconSpan('list');
            }
            if(this.can(data.show)) {
                fields += this.getIconSpan('eye');
            }
            if(this.can(data.create)) {
                fields += this.getIconSpan('plus-circle');
            }
            if(this.can(data.update)) {
                fields += this.getIconSpan('pencil');
            }
            if(this.can(data.delete)) {
                fields += this.getIconSpan('trash');
            }
            return fields;
        },
        getIconSpan(icon) {
            return ` <i class="fa fa-${icon}" aria-hidden="true"></i> `;
        }
    },
}
