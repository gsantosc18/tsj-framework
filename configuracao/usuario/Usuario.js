export default {
    data() {
        return {
            dados: [],
            currentPage: 1,
            totalPage: 0,
            headers: ['...','perfils','nome','Cadastro','situação','Opçoes'],
            columns: [
                {
                    title: '<i class="fa fa-pencil"></i>',
                    component: 'btn',
                    can: ()=>(this.$permission.get('usuario').can('show')),
                    event: {
                        click: (data) => {
                            this.$abaService.addComponent(
                                `Editar Usuário: ${data.nome}`,'editusuario',{ dados:data }
                            );
                        }
                    },
                    chaves: (data) => {
                        return {
                            data: data,
                            type : "link",
                            size: "xs"
                        };
                    }
                },
                {
                    title: '<i class="fa fa-lock"></i>',
                    component: 'btn',
                    can: ()=>(this.$permission.get('usuarioperfil').can('select')),
                    event: {
                        click: (data) => {
                            this.$abaService.addComponent(
                                `Perfis Usuário: ${data.nome}`,'usuarioperfil',{ usuario: data }
                            );
                        }
                    },
                    chaves: (data) => {
                        return {
                            data: data,
                            type : "warning",
                            size: "xs"
                        };
                    }
                },
                'nome',
                'data_cadastro',
                'status',
                {
                    title: '<i class="fa fa-trash"></i>',
                    component: 'btn',
                    can: ()=>(this.$permission.get('usuario').can('delete')),
                    event: {
                        click: (data) => {
                            this.$notification.confirm('warning','Deseja apagar o usuário '+data.nome,'Ao confirmar você concorda em remover o usuário '+data.nome,r=>{
                                this.$externalService.delete('usuario',data.id,{},resp=>{
                                    this.$notification.show("success","Usuário removido com sucesso!");
                                    this.find();
                                });
                            });
                        }
                    },
                    chaves: (data) => {
                        return {
                            data: data,
                            type : "danger",
                            size: "xs"
                        };
                    }
                }
            ]
        }
    },
    watch: {
        currentPage: function () {
            this.find();
        }
    },
    mounted() {
        this.find();
        this.$broadcast.$on('updateUsuario',()=>this.find());
    },
    methods: {
        find() {
            const data = {
                start: this.currentPage - 1,
                qtd: 15
            };
            this.$externalService.get('usuario',data,r=>{
                this.dados=this.trataData(r.data.data);
                this.totalPage = r.data.pages;
                this.sumTotalRecords = r.data.count;
            });
        },
        trataData(data) {
            return data.map((e)=>{
                return {...e,
                    status:this.getBadgeStatus(e.ativo),
                    data_cadastro: this.formatData(e.created_at)
                }
            });
        },
        getBadgeStatus(status) {
            if(status == 1) {
                return '<span class="label label-success btn-sm">ATIVO</span>';
            }
            return '<span class="label label-danger">INATIVO</span>';
        },
        formatData(date) {
            if(!date) return "";
            return moment(date).format('DD/MM/YYYY HH:mm:ss');
        }
    },
}
